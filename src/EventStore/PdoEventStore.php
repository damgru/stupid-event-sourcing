<?php /** @noinspection SqlResolve */

namespace StupidEventSourcing\EventStore;

use DateTimeImmutable;
use StupidEventSourcing\AggregateEvent;

class PdoEventStore implements EventStore
{
    protected \PDO $pdo;
    private string $eventsTableName;

    public function __construct(\PDO $pdo, string $eventsTableName = 'events')
    {
        $this->pdo = $pdo;
        $this->eventsTableName = $eventsTableName;
    }

    public function add(AggregateEvent ...$events): void
    {
        $sql = <<<SQL
            INSERT INTO {$this->eventsTableName} (aggregate_id, name, payload, created, version, category) 
            VALUES             
SQL;
        $sql .= implode(',', array_fill(0, count($events), '(?,?,?,?,?,?)'));
        $stmt = $this->pdo->prepare($sql);
        $params = [];
        foreach ($events as $event)
        {
            array_push($params,
                $event->aggregateId(),
                $event->name(),
                json_encode($event->payload()),
                $event->created()->format('Y-m-d H:i:s.u'),
                $event->version(),
                $event->category()
            );
        }

        $stmt->execute($params);
    }

    public function getAllEvents(): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM {$this->eventsTableName} 
ORDER BY created, version ASC;
SQL;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch()) {
            yield $this->eventFromRow($row);
        }
    }

    public function get(string $aggregateId): iterable
    {
        $sql = <<<SQL
SELECT * FROM {$this->eventsTableName} WHERE aggregate_id = ? ORDER BY created, version;
SQL;
        $stmt = $this->pdo->prepare($sql);

        $stmt->execute([
            $aggregateId
        ]);
        while ($row = $stmt->fetch()) {
            yield $this->eventFromRow($row);
        }
    }

    public function getNewerThanVersion(string $aggregateId, int $version, int $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM {$this->eventsTableName} 
WHERE aggregate_id = :aggregate_id 
  AND version > :version 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('aggregate_id', $aggregateId);
        $stmt->bindParam('version', $version);

        if($limit > 0) {
            $stmt->bindParam('limit', $limit);
        }

        $stmt->execute();
        while ($row = $stmt->fetch()) {
            yield $this->eventFromRow($row);
        }
    }

    public function getOlderAndEqualThanVersion(string $aggregateId, int $version, int $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM {$this->eventsTableName} 
WHERE aggregate_id = :aggregate_id 
  AND version <= :version 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam('aggregate_id', $aggregateId);
        $stmt->bindParam('version', $version);

        if($limit > 0) {
            $stmt->bindParam('limit', $limit);
        }

        $stmt->execute();
        while ($row = $stmt->fetch()) {
            yield $this->eventFromRow($row);
        }
    }

    public function getAllNewerThanDate(DateTimeImmutable $timeImmutable, int $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM {$this->eventsTableName} 
WHERE created > :created 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue('created', $timeImmutable->format('Y-m-d H:i:s.u'));

        if($limit > 0) {
            $stmt->bindValue('limit', $limit);
        }

        $stmt->execute();
        while ($row = $stmt->fetch()) {
            yield $this->eventFromRow($row);
        }
    }

    public function getAllOlderAndEqualThanDate(DateTimeImmutable $timeImmutable, int $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM {$this->eventsTableName} 
WHERE created <= :created 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue('created', $timeImmutable->format('Y-m-d H:i:s.u'));

        if($limit > 0) {
            $stmt->bindValue('limit', $limit);
        }

        $stmt->execute();
        while ($row = $stmt->fetch()) {
            yield $this->eventFromRow($row);
        }
    }

    public function beginTransaction(): void
    {
        $this->pdo->beginTransaction();
    }

    public function commitTransaction(): void
    {
        $this->pdo->commit();
    }

    public function rollbackTransaction(): void
    {
        $this->pdo->rollBack();
    }

    public function init()
    {
        $sql = <<<SQL
CREATE TABLE {$this->eventsTableName} (
    id serial PRIMARY KEY not null ,
    aggregate_id text not null ,
    version int not null ,
    name text not null ,
    payload jsonb not null,
    created timestamp default now(),
    category text null
);
SQL;
        $this->executeSql($sql);

        $sql = <<<SQL
CREATE INDEX ON {$this->eventsTableName} (aggregate_id);
SQL;

        $this->executeSql($sql);

        $sql = <<<SQL
CREATE INDEX ON {$this->eventsTableName} (created);
SQL;

        $this->executeSql($sql);

    }

    protected function executeSql(string $sql, array $params = []) : \PDOStatement
    {
        $statement = $this->pdo->prepare($sql);
        $statement->execute($params);

        if($statement->errorCode() != 0) {
            throw new \RuntimeException('Błąd zapytania do bazy danych...');
        }

        return $statement;
    }

    /**
     * @param $row
     * @return AggregateEvent
     */
    protected function eventFromRow($row): AggregateEvent
    {
        return new AggregateEvent(
            $row['aggregate_id'],
            intval($row['version']),
            $row['name'],
            json_decode($row['payload'], true),
            $row['category']
        );
    }
}