<?php
namespace StupidEventSourcing\EventStore;

use DateTimeImmutable;
use StupidEventSourcing\AggregateEvent;

interface EventStore
{
    public function init();

    public function add(AggregateEvent ...$events): void;

    public function getAllEvents() : iterable;
    public function getAllNewerThanDate(DateTimeImmutable $timeImmutable, int $limit = 0) : iterable;
    public function getAllOlderAndEqualThanDate(DateTimeImmutable $timeImmutable, int $limit = 0) : iterable;

    public function get(string $aggregateId) : iterable;
    public function getNewerThanVersion(string $aggregateId, int $version, int $limit = 0) : iterable;
    public function getOlderAndEqualThanVersion(string $aggregateId, int $version, int $limit = 0) : iterable;

    public function beginTransaction() : void;
    public function commitTransaction() : void;
    public function rollbackTransaction() : void;
}