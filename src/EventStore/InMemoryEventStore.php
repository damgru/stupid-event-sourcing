<?php

namespace StupidEventSourcing\EventStore;

use DateTimeImmutable;
use StupidEventSourcing\AggregateEvent;

final class InMemoryEventStore implements EventStore
{
    /** @var AggregateEvent[] */
    private array $events = [];

    /** @var AggregateEvent[] */
    private bool $isTransaction = false;
    private array $transaction = [];

    public function init()
    {
        $this->events = [];
    }

    public function add(AggregateEvent ...$events): void
    {
        if($this->isTransaction) {
            $this->transaction = array_merge($this->transaction, $events);
        }
        else {
            $this->events = array_merge($this->events, $events);
        }
    }

    public function getAllEvents(): iterable
    {
        return array_merge($this->events, $this->transaction);
    }

    public function getAllNewerThanDate(DateTimeImmutable $timeImmutable, int $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->getAllEvents() as $event) {
            if ($event->created() > $timeImmutable) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    public function getAllOlderAndEqualThanDate(DateTimeImmutable $timeImmutable, int $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->getAllEvents() as $event) {
            if ($event->created() <= $timeImmutable) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    public function get(string $aggregateId): iterable
    {
        foreach ($this->getAllEvents() as $event) {
            if ($event->aggregateId() === $aggregateId) {
                yield $event;
            }
        }
    }

    public function getNewerThanVersion(string $aggregateId, int $version, int $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->getAllEvents() as $event) {
            if ($event->aggregateId() === $aggregateId && $event->version() > $version) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    public function getOlderAndEqualThanVersion(string $aggregateId, int $version, int $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->getAllEvents() as $event) {
            if ($event->aggregateId() === $aggregateId && $event->version() <= $version) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    public function beginTransaction(): void
    {
        $this->isTransaction = true;
    }

    public function commitTransaction(): void
    {
        $this->events = array_merge($this->events, $this->transaction);
        $this->isTransaction = false;
        $this->transaction = [];
    }

    public function rollbackTransaction(): void
    {
        $this->isTransaction = false;
        $this->transaction = [];
    }
}