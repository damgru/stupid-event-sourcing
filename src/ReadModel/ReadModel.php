<?php

namespace StupidEventSourcing\ReadModel;

interface ReadModel
{
    /**
     * code/migration needed to initialize your read model
     * eg. creating databases/tables
     */
    public function init();

    /**
     * cleanup from readmodel. Used, when we want to recreate read model
     */
    public function drop();

    /**
     * responsible for pulling events and update all information in read model
     */
    public function update();

    /**
     * this method is used by Read Model Manager to inform you, when you last updated.
     * @param \DateTimeImmutable $eventTime
     * @return mixed
     */
    public function withLastEventTime(\DateTimeImmutable $eventTime);

    /**
     * getter for lastEventTime
     * @return \DateTimeImmutable
     */
    public function lastEventTime() : \DateTimeImmutable;
}