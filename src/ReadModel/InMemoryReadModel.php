<?php

namespace StupidEventSourcing\ReadModel;

use StupidEventSourcing\AggregateEvent;
use StupidEventSourcing\EventStore\EventStore;

abstract class InMemoryReadModel implements ReadModel
{
    protected EventStore $eventStore;
    /** @var \DateTimeImmutable */
    protected $lastEventTime;

    public function update()
    {
        foreach ($this->eventStore->getAllNewerThanDate($this->lastEventTime) as $event)
        {
            /** @var $event AggregateEvent */
            $this->apply($event);
            $this->lastEventTime = $event->created();
        }
    }

    abstract protected function apply(AggregateEvent $event);

    public function withLastEventTime(\DateTimeImmutable $eventTime)
    {
        $this->lastEventTime = $eventTime;
    }

    public function lastEventTime(): \DateTimeImmutable
    {
        return $this->lastEventTime;
    }
}