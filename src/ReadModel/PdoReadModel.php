<?php

namespace StupidEventSourcing\ReadModel;

use StupidEventSourcing\AggregateEvent;
use StupidEventSourcing\EventStore\EventStore;

abstract class PdoReadModel implements ReadModel
{
    protected \PDO $connection;
    protected EventStore $eventStore;
    /** @var \DateTimeImmutable */
    protected $lastEventTime;

    /**
     * PdoReadModel constructor.
     * @param \PDO $connection
     * @param EventStore $eventStore
     * @param \DateTimeImmutable $lastEventTime
     */
    public function __construct(\PDO $connection, EventStore $eventStore, ?\DateTimeImmutable $lastEventTime = null)
    {
        $this->connection = $connection;
        $this->eventStore = $eventStore;
        $this->lastEventTime = empty($lastEventTime)
            ? \DateTimeImmutable::createFromFormat('Y-m-d', '1980-01-01')
            : $lastEventTime;
    }

    public function update()
    {
        foreach ($this->eventStore->getAllNewerThanDate($this->lastEventTime) as $event)
        {
            /** @var $event AggregateEvent */
            $this->apply($event);
            $this->lastEventTime = $event->created();
        }
    }

    public function withLastEventTime(\DateTimeImmutable $eventTime)
    {
        $this->lastEventTime = $eventTime;
    }

    public function lastEventTime(): \DateTimeImmutable
    {
        return $this->lastEventTime;
    }

    abstract protected function apply(AggregateEvent $event): void;

    protected function executeSql(string $sql, array $params = [])
    {
        $this->connection->prepare($sql)->execute($params);
    }

    protected function fetchAll(string $sql, array $params = []): iterable
    {
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }
}