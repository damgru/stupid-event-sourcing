<?php
namespace StupidEventSourcing;

use StupidEventSourcing\EventStore\EventStore;

class AggregateRootRepository
{
    private EventStore $eventStore;

    public function __construct(EventStore $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    public function save(AggregateRoot $aggregateRoot)
    {
        $this->eventStore->beginTransaction();
        $this->eventStore->add(...$aggregateRoot->getUncommitedEvents());
        $this->eventStore->commitTransaction();
    }

    public function load(string $className, string $aggregateId): AggregateRoot
    {
        return $className::fromEvents($this->eventStore->get($aggregateId));
    }

    public function loadVersion(string $className, string $aggregateId, int $version): AggregateRoot
    {
        return $className::fromEvents($this->eventStore->getOlderAndEqualThanVersion($aggregateId, $version));
    }
}