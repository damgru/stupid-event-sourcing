<?php
namespace StupidEventSourcing;

abstract class AggregateRoot
{
    protected int $version = 0;
    /** @var AggregateEvent[] */
    protected array $events = [];

    /**
     * Most common approach is to use uuid as aggregateId, but you can use any string
     * @return string
     */
    abstract public function aggregateId() : string ;
    abstract public function apply(AggregateEvent $event);

    public static function fromEvents(AggregateEvent ...$events): AggregateRoot
    {
        $root = new static();
        $root->replay($events);
        return $root;
    }

    /**
     * @param iterable<AggregateEvent> $historyEvents
     */
    protected function replay(iterable $historyEvents)
    {
        foreach ($historyEvents as $event)
        {
            $this->version++;
            $this->apply($event);
        }
    }

    /**
     * @return iterable<AggregateEvent>
     */
    public function getUncommitedEvents() : iterable
    {
        return $this->events;
    }

    public function markEventsAsCommited(): void
    {
        $this->events = [];
    }

    protected function record(AggregateEvent $event): void
    {
        $this->version++;
        $this->events[] = $event->withVersion($this->version);
        $this->apply($event);
    }

    protected function recordEvent(string $eventName, array $payload = [], string $category = '')
    {
        $this->record(AggregateEvent::create(
            $eventName,
            $this->aggregateId(),
            $payload,
            null,
            $category
        ));
    }

    public function __toString()
    {
        return get_class($this) . "({$this->aggregateId()})";
    }

    public function version() : int
    {
        return $this->version;
    }


}