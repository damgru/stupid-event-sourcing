<?php
namespace StupidEventSourcing;

use DateTimeImmutable;

class AggregateEvent
{
    protected string $aggId;
    protected int $version;
    /** @var string unique name across application */
    protected string $name;
    protected array $payload = [];
    protected ?DateTimeImmutable $created;
    protected string $category = '';

    /**
     * Event constructor.
     * @param string $aggId
     * @param int $version
     * @param string $name
     * @param array $payload
     * @param DateTimeImmutable|null $created
     * @param string $category
     */
    public function __construct(
        string $aggId,
        int $version,
        string $name,
        array $payload = [],
        ?DateTimeImmutable $created = null,
        string $category = ''
    ) {
        $this->aggId = $aggId;
        $this->version = $version;
        $this->name = $name;
        $this->payload = $payload;
        $this->created = $created ?? DateTimeImmutable::createFromFormat('0.u00 U', microtime());
        $this->category = $category;
    }

    public static function create(string $name, string $aggId, array $payload = [], $created = null, string $category = '') : AggregateEvent
    {
        return new static($aggId, 1, $name, $payload, $created, $category);
    }

    public function withVersion(int $version): self
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function aggregateId(): string
    {
        return $this->aggId;
    }

    /**
     * @return int
     */
    public function version(): int
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function payload(): array
    {
        return $this->payload;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function created(): ?DateTimeImmutable
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function category(): string
    {
        return $this->category;
    }
}