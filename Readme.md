# Stupid Event Sourcing

Simple and Stupid implementation for event sourcing. Good for fast prototyping and **bad for production**.

Implementation may change over time. better aproach is to copy sources and adapt to Your needs.

## Install
```
composer require damgru/stupid-event-sourcing
```

## Usage
### 1. configure event store
```php
// buildIn inmemory event store - for tests
$eventStore = new \StupidEventSourcing\EventStore\InMemoryEventStore();
// pdo event store...
$pdo = new PDO('your_connection');
$eventsTableName = 'my-events-table'; //default name 'events' 
$eventStore = new \StupidEventSourcing\EventStore\PdoEventStore($pdo, $eventsTableName);
```
or write your own implementation, that implements interface

```php
class MyEventStore implements \StupidEventSourcing\EventStore\EventStore {}
```

### 2. Create your aggregate

```php
use StupidEventSourcing\AggregateEvent;

class ExampleWallet extends \StupidEventSourcing\AggregateRoot
{
    private float $balance = 0;
    private string $aggregateId;
    
    public function aggregateId() : string {
        // create property for aggregateId and return it here
        return $this->aggregateId;
    }
    
    public static function createNewWallet(string $id): ExampleWallet
    {
        $wallet = new ExampleWallet();
        // every time you want to change something, you record new event
        $wallet->record(AggregateEvent::create('wallet-created', $id));
        return $wallet;
    }
    
    // only place, where you set properties
    public function apply(AggregateEvent $event){
        switch ($event->name()) {
            case 'wallet-created':
                $this->aggregateId = $event->aggregateId();
                break;
            case 'money-added':
                $this->balance += $event->payload()['amount'];
                break;
            case 'money-substracted':
                $this->balance -= $event->payload()['amount'];
                break;
        }
    }
    
    // domain action
    public function addMoreMoney(float $money)
    {
        //deny if possible
        if ($money <= 0) {
            throw new Exception('sneaky hacker!');
        }
        
        //record success
        $this->record(AggregateEvent::create('money-added', $this->aggregateId, ['amount' => $money]));
    }
    
    // domain action
    public function withdraw(float $money)
    {
        if ($money <= 0) {
            throw new Exception('sneaky hacker!');
        }
        if ($this->balance < $money) {
            throw new Exception('dude, you broke!');
        }
        
        $this->record(AggregateEvent::create('money-substracted', $this->aggregateId));
    }
}
```